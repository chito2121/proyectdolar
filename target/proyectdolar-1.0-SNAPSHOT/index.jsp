<%@page import="javax.ws.rs.core.Response"%>
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <%
        Calendar fechaHoy = Calendar.getInstance();
        int hoy = fechaHoy.get(Calendar.DAY_OF_MONTH);
        int mes = fechaHoy.get(Calendar.MONTH) + 1;
        int anio = fechaHoy.get(Calendar.YEAR);

    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="width: 300px; margin: 10px auto;">
        <form action="api/fecha?" method="get">
            <fieldset>
                <legend>Consultar Dolar</legend>
                <label for="dia">Dia</label>
                <br>
                <input type="number" name="dia" value="<%= hoy%>" max="31" min="1">
                <br>
                <label for="mes">Mes</label>
                <br>
                <input type="number" name="mes" value="<%= mes%>" max="12" min="1">
                <br>
                <label for="anio">Año</label>
                <br>
                <input type="number" name="anio" value="<%= anio%>" max="<%= anio%>">
                <br>
                <br>
                <input type="submit" value="Consultar">
            </fieldset>
        </form>
    </body>
</html>
